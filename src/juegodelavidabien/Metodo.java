package juegodelavidabien;

import java.util.Scanner;

import java.util.Random;


/**
 * 
 * @author joelm
 *
 */
public class Metodo {
	static Scanner src = new Scanner(System.in);
	static int max=100;
/**
 * 
 * @param args el metode main es per seleccionar les coses que es vol fer
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcio;
		String jugador = null;
		boolean definit = false;
		Casella[][] taulell=null;
	int files=0;
	int cols=0;
	
		//int [] dificultat= new int[3];  //indica les minesdificultat[0]/files(dificultat[1])/cols(dificultat[2])
		do {
			opcio = menu();
			switch (opcio) {
				case 1: 

					 files=obtenirmesura();
					 cols=obtenirmesura();
					taulell=Board(files,cols);
					break;
				case 2:
					visualitzaTauler(taulell,files,cols);
					break;
				case 3:
					taulell=iterar(taulell,files,cols);
					break;
				case 0: System.out.println("Moltes gracies per jugar\n\nAdeu!!!");
						break;
				default: System.out.println("Error, opci� incorrecta. Torna-ho a provar..");
			}
		} while (opcio != 0);
	
	}
	/**
	 * 
	 * @return
	 */
		private static int obtenirmesura() {
		// TODO Auto-generated method stub
			int valor=0;
			 boolean correcte= false;
			do {
				try {
					System.out.println("Intoduir valor entre 0 i"+ max);
					valor=src.nextInt();
					if(valor<=max) {
						correcte=true;
					}
				}
				catch(Exception e) {
					System.out.println("Intoduir valor entre 0 i"+ max);
					src.nextLine();
				}
			}while(!correcte);
			return valor;
	}
		/**
		 * 
		 * @param taulell
		 * @param files
		 * @param cols
		 * @return
		 */
		private static Casella [][] iterar(Casella [][] taulell, int files, int cols) {
		// TODO Auto-generated method stub
			int vives=0;
			int vegades;
			Casella [][] noutaulell= new Casella [files][cols];
			System.out.println("Indica numero de vegades que vols iterar");
			vegades=src.nextInt();
			while(vegades>0) {
			for(int i=0; i<files; i++) {
				for(int j=0;j<cols;j++) {
					int cont = 0;

					if (i-1 >=0 && j-1 >=0 && taulell [i-1][j-1] == Casella.Viu) cont++; //System.out.println("[i-1][j-1]");
					if (i-1 >=0 && taulell [i-1][j] == Casella.Viu) cont++; //System.out.println("[i-1][j]");
					if (i-1 >=0 && j+1 < cols && taulell [i-1][j+1] == Casella.Viu) cont++; //System.out.println("[i-1][j+1]");
					if (j-1 >=0 && taulell [i][j-1] == Casella.Viu) cont++; //System.out.println("[i][j-1]");
					if (j+1 < cols && taulell [i][j+1] == Casella.Viu) cont++; //System.out.println("[i][j+1]");
					if (i+1 < files && j-1 >=0 && taulell [i+1][j-1] == Casella.Viu) cont++; //System.out.println("[i+1][j-1]");
					if (i+1 < files && taulell [i+1][j] == Casella.Viu) cont++; //System.out.println("[i+1][j]");
					if (i+1 < files && j+1 < cols && taulell [i+1][j+1] == Casella.Viu) cont++; //System.out.println("[i+1][j+1]");

					if (taulell[i][j]==Casella.Mort) {
					if (cont==3) noutaulell[i][j] = Casella.Viu;
					else noutaulell[i][j] = Casella.Mort;
					}

					else {
					if (cont==2 || cont==3) noutaulell[i][j] = Casella.Viu;
					else noutaulell[i][j] = Casella.Mort;
					}
					
				}
			}
			visualitzaTauler2(noutaulell,files,cols);
			vegades--;
			}
			return noutaulell;
	}
		/**
		 * 
		 * @param noutaulell
		 * @param files
		 * @param cols
		 */
		private static void visualitzaTauler2(Casella[][] noutaulell, int files, int cols) {
			// TODO Auto-generated method stub
			System.out.printf("   ");
			for(int i=0; i<cols; i++) {
				System.out.printf("%4d",i+1);
				
			}
			System.out.println();
			for(int i=0; i<files; i++) {
				System.out.printf("%4d",i+1);
				for(int j=0;j<cols;j++) {
					if(noutaulell[i][j]==Casella.Viu) {
						System.out.printf("%4c",'*');
					}
					else {
						if(noutaulell[i][j]==Casella.Mort) {
							System.out.printf("%4c",'-');
						}
					}
					}
				System.out.println();
				}
		}
		/**
		 * 
		 * @param taulell es la matriu on esta guardades les posicions amb les caselles vives i mortes
		 * @param files
		 * @param cols
		 */
		private static void visualitzaTauler( Casella [][] taulell, int files, int cols) {
		// TODO Auto-generated method stub
			System.out.printf("   ");
			for(int i=0; i<cols; i++) {
				System.out.printf("%4d",i+1);
				
			}
			System.out.println();
			for(int i=0; i<files; i++) {
				System.out.printf("%4d",i+1);
				for(int j=0;j<cols;j++) {
					if(taulell[i][j]==Casella.Viu) {
						System.out.printf("%4c",'*');
					}
					else {
						if(taulell[i][j]==Casella.Mort) {
							System.out.printf("%4c",'-');
						}
					}
					}
				System.out.println();
				}
			
		
	}
		/**
		 * 
		 * @param files son les files que te el projecte
		 * @param cols son les columnes que te el projecte 
		 * @return taulell que es la matriu on indiquem totes les caselles vives i mortes
		 */
		private static Casella[][] Board(int files,int cols) {
		// TODO Auto-generated method stub
			Casella [][] taulell =new Casella[files][cols];
			//mostrem els numeros en horitzontal
			Random rnd= new Random();
			for(int i=0; i<files; i++) {
				for(int j=0;j<cols;j++) {
					taulell[i][j]=Casella.Mort;
				}
			}
			
		int	num=(int)((rnd.nextDouble()*0.25+0.25)*(files*cols));
			
		for(;num>0;num--) {
			boolean sortir=false;
			do {
				int fila=rnd.nextInt(files);
				int col=rnd.nextInt(cols);
				if(taulell[fila][col]==Casella.Mort) {
					sortir=true;
					taulell[fila][col]=Casella.Viu;
				}
			}while(sortir==false);
		}

			return taulell;
			
		}
	/**
	 * 
	 * @return op un int que posem per indicar la opcio que es vol
	 */
		public static int menu() {
			// TODO Auto-generated method stub
			int op = 0;
			boolean valid = false;
			
			System.out.println("1.- Inicialitzar");
			System.out.println("2.- Visualitzar");
			System.out.println("3.- iterar");
			System.out.println("0.- Sortir");
			System.out.println("\nEscull una opci�: ");
			
			do {
				try {
					op = src.nextInt();
					valid = true;
				}
				catch (Exception e) {
					System.out.println("Error, has de posar un numero enter");
					src.nextLine();
				}
			} while(!valid);
			
			return op;
		}
	}