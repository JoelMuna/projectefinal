
package juegodelavidabien;
/**
 * 
 * @author joelm
 * @version 2021-06
 * @since 8-3-2021
 *
 */
public enum Casella {
	/**
	 * la casella esta viva
	 */
	Viu,
	/**
	 * la casella esta morta
	 */
	Mort;


}
